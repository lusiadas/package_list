set -l cmd (basename (status -f) | cut -d '.' -f 1)
complete -xc $cmd -s a -l add -d 'Install packages and add them to the package list'
complete -xc $cmd -s r -l remove -d 'Uninstall packages and remove them from the package list'
complete -xc $cmd -s f -l file -d 'Set the directory to place the package list file'
complete -xc $cmd -s f -l file -a 'tails' -d 'Set the directory to place the package list file'
complete -fc $cmd -s l -l list -d 'Print the contents of the package list'
complete -fc $cmd -s h -l help -d 'Display instructions'
